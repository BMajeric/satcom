import json
import random

station = {
    "station_name": "",
    "station_id": "",
    "antennas": [],
    "status": "",
    "description": ""
}

with open("stations.txt", "w") as file:
    for i in range(1, 21):
        station["station_name"] = "station_" + str(i)
        station["station_id"] = str(i)
        station["antennas"] = []
        number_of_antennas = random.randint(1, 5)
        # add antennas
        for j in range(number_of_antennas):
            antenna = {
                "min_freq": "",
                "max_freq": ""
            }
            antenna["min_freq"] = i + j * random.randint(0, 3)
            antenna["max_freq"] = antenna["min_freq"] + random.randint(5, 15)
            station["antennas"].append(antenna)
        
        station["status"] = "Online"
        station["description"] = "SatNOGS station_" + str(i)

        file.write(json.dumps(station))
        file.write("\n")