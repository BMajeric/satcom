from django.shortcuts import render, redirect, get_object_or_404
from .forms import SatelliteForm
from .models import Satellite
from django.contrib.auth.decorators import login_required
from users.models import CustomUser
from links.models import Link
import json
# Create your views here.
@login_required()
def delete_satellite_view(request, id):
    obj = get_object_or_404(Satellite, id=id)
    if (request.user == obj.owner):
        
        if request.method == 'POST':
            obj.delete()
            return redirect('../../../')

        context = {
            'user': request.user,
            "object": obj
        }

        return render(request, 'satellite_delete.html', context)
    else:
        return redirect('/')
@login_required()
def save_satellite_details_view(request,id):
    if not request.user.is_sat_admin:
        return redirect('/')
    if (request.POST):
        new_name = request.POST['satname']
        new_links = request.POST.getlist('links')
        obj = Satellite.objects.get(id=id)
        obj.name = new_name
        obj.link.set(new_links)
        obj.save()
        return redirect('../../')
    
@login_required()
def edit_satellite_details_view(request, id):
    obj = get_object_or_404(Satellite, id=id)
    if (request.user == obj.owner):      
        links = list(obj.link.all())
        all_links = list(Link.objects.all())
        context = {
            'user': request.user,
            "object": obj,
            'links' : links,
            'all_links':all_links
            
        }

        return render(request, 'edit_satellite_details.html', context)
    else:
        return redirect('/')
    
def satellites_details_view(request):
    links = dict()
    objs = Satellite.objects.filter(owner = request.user)
    if (request.user.is_sat_admin):
        
        for sat in objs:
            linksSat = list(sat.link.all())
            link_names = []
            for l in linksSat:
                link_names.append(l.name)
            link_names = ', '.join(link_names)
            links[sat.id] = link_names
       
        context = {
            'user': request.user,
            'satellites': Satellite.objects.filter(owner = request.user),
            'links': links
        }
        return render(request, 'satellites_details.html', context)
    else:
        return redirect('/')
    
@login_required()
def satellite_create_view(request):
    # only sat admin is allowed to view this page
    if not request.user.is_sat_admin:
        return redirect('../')

    form = SatelliteForm(request.POST or None)
    if form.is_valid():
        new_satelitte = Satellite.objects.create(
            name = request.POST["name"],
            owner = CustomUser.objects.get(id=request.user.id)
        )
        new_satelitte.link.set(request.POST.getlist("link"))
        return redirect('../')

    satellites = Satellite.objects.all()

    context = {
        'user': request.user,
        'form': form,
        'satellites': satellites
    }

    return render(request, "satellite_create.html", context)
