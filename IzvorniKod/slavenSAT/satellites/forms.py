from django import forms
from .models import Satellite
from users.models import CustomUser
from links.models import Link

class SatelliteForm(forms.Form):

    name = forms.CharField(max_length=100, required=True, label="Ime satelita")
    link = forms.ModelMultipleChoiceField(
        widget=forms.CheckboxSelectMultiple,
        queryset=Link.objects.all(),
        required=True,
        label="Linkovi satelita"
    )
