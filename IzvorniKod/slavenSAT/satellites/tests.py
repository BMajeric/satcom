from django.test import TestCase
from satellites.models import Satellite
from users.models import CustomUser
from links.models import Link

# Create your tests here.
class TestSatellite(TestCase):

    def setUp(self):
        link = Link.objects.create(name="test_link", frequency=100)
        satellite_admin = CustomUser.objects.create(email="satellite_sat@admin", is_staff=True)
        satellite = Satellite.objects.create(owner=satellite_admin, name="melupo_test")
        satellite.link.add(link)
    

    def test_satellites_printout(self):
        satellite = Satellite.objects.get(name="melupo_test")
        self.assertEqual(str(satellite), satellite.name + ": " + 
                                        str(satellite.owner) + ": " +
                                        str(list(satellite.link.all())))

    def test_add_link_to_satellite(self):
        satellite = Satellite.objects.get(name="melupo_test")

        link = Link.objects.get(name="test_link")
        link_new = Link.objects.create(name="test_link_new", frequency=100)
        satellite.link.add(link_new)

        self.assertEqual(link in list(satellite.link.all()), True)
        self.assertEqual(link_new in list(satellite.link.all()), True)
