from django.db import models
from users.models import CustomUser
from links.models import Link

class Satellite(models.Model):
    owner = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    name = models.CharField(max_length=100, blank=False)
    link = models.ManyToManyField(Link)

    def __str__(self):
        return self.name + ": " + str(self.owner) + ": " + str(list(self.link.all()))
