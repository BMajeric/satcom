from selenium import webdriver
import unittest
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import Select


class SeleniumTests(unittest.TestCase):

    def setUp(self) -> None:
        s=Service(ChromeDriverManager().install())
        self.driver = webdriver.Chrome(service=s)
        self.url = "http://localhost:8080/"
        return super().setUp()
    
    
    def succesful_login(self):
        driver = self.driver
        driver.get(self.url)
        # login as admin
        usr_name_field = driver.find_element("id", "id_username")
        usr_name_field.send_keys("s@t.com")
        pwd_field = driver.find_element("id", "id_password")
        pwd_field.send_keys("slaven")
        log_in = driver.find_element("id", "login")
        log_in.click()
        self.assertEqual(driver.current_url, self.url)
    

    def create_satellite(self, sat_given_name):
        driver = self.driver
        new_sat_btn = driver.find_element("id", "new_sat")
        new_sat_btn.click()
        self.assertEqual(driver.current_url, "http://localhost:8080/satellites_create/")

        # sat name
        sat_name = driver.find_element("id", "id_name")
        sat_name.send_keys(sat_given_name)
        # link
        link = driver.find_element("id", "id_link_3")
        link.click()
        # save
        save_btn = driver.find_element("id", "save")
        save_btn.click()

        self.assertEqual(driver.current_url, self.url)


    def test_create_new_satellite(self):
        self.succesful_login()
        self.create_satellite("melupo_1")
        self.driver.close()


    def test_unsuccessful_login(self):
        driver = self.driver
        driver.get(self.url)

        login_url = driver.current_url
        # login as admin
        usr_name_field = driver.find_element("id", "id_username")
        usr_name_field.send_keys("s@t.com")
        pwd_field = driver.find_element("id", "id_password")
        pwd_field.send_keys("invalid_pwd")
        log_in = driver.find_element("id", "login")
        log_in.click()
        self.assertEqual(driver.current_url, login_url)
        driver.close()    


    def test_successful_login(self):
        self.succesful_login()
        self.driver.close()
    

    def send_message(self, msg):
        driver = self.driver
        # click new message
        new_msg_btn = driver.find_element("id", "new_msg")
        new_msg_btn.click()

        # find first satellite
        driver.find_element("id", "1").click()

        self.assertEqual(driver.current_url, "http://localhost:8080/communications_create/1/")

        # msg_text
        msg_field = driver.find_element("id", "id_msg_text")
        msg_field.send_keys(msg)

        # select link
        select = Select(driver.find_element("id", 'id_link'))
        select.select_by_visible_text("Link 4 - 7 GHz")

        # send messagge
        send_btn = driver.find_element("id", "id_send")
        send_btn.click()

        self.assertEqual(driver.current_url, "http://localhost:8080/")


    def test_send_message(self):
        self.succesful_login()
        self.create_satellite("melupo_2")
        self.send_message("test message")
        self.driver.close()
    


if __name__ == '__main__':
    unittest.main()