"""slavenSAT URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
# from django.conf.urls import url
from django.contrib import admin
from django.urls import path
from satellites.views import save_satellite_details_view,satellite_create_view, satellites_details_view, edit_satellite_details_view, delete_satellite_view
from django.contrib.auth.views import LoginView, PasswordChangeView
from users.views import my_view, create_users_view, logout_view, change_password
from communications.views import sat_select_view, communication_finish_view,communication_create_view, communication_details_view, communication_delete, stations_select_view

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/login/', LoginView.as_view()),
    path('accounts/logout/', logout_view),
    path('password-reset/', change_password),
    path('', my_view),
    path('users_create/', create_users_view),
    path('satellites_create/', satellite_create_view),
    path('satellites_select/', sat_select_view),
    path('communications_create/<int:id>/', communication_create_view),
    path('communication/<int:id>/', communication_details_view),
    path('communication/<int:id>/delete/', communication_delete),
    path('communications_create/<int:id>/stations_select/', stations_select_view),
    path('communications_create/<int:id>/finish', communication_finish_view),
    path('satellites_details/', satellites_details_view),
    path('satellites_details/edit/<int:id>/', edit_satellite_details_view),
    path('satellites_details/edit/<int:id>/delete/', delete_satellite_view),
    path('satellites_details/edit/<int:id>/save', save_satellite_details_view)
    
]



