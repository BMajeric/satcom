# uvicorn mockup_server:app --reload
import random
import json
from fastapi import FastAPI
from pydantic import BaseModel


class Message(BaseModel):
    message: str
    station_id: str
    satellite_id: str

    def __str__(self):
        return self.message


responses = [
    "successful communication",
    "message received successfully",
    "010101101011011010100100010110110110110101010111",
    "zeza",
    "successful communication",
    "successful communication",
    "successful communication",
    "successful communication",
    "successful communication",
    "successful communication",
    "melupo"
]

stations_list = [
    '{"station_name": "station_1", "station_id": "1", "antennas": [{"min_freq": 1, "max_freq": 7}, {"min_freq": 2, "max_freq": 13}], "status": "Online", "description": "SatNOGS station_1"}',
    '{"station_name": "station_2", "station_id": "2", "antennas": [{"min_freq": 1, "max_freq": 14}], "status": "Online", "description": "SatNOGS station_2"}',
    '{"station_name": "station_3", "station_id": "3", "antennas": [{"min_freq": 3, "max_freq": 12}], "status": "Online", "description": "SatNOGS station_3"}',
    '{"station_name": "station_4", "station_id": "4", "antennas": [{"min_freq": 4, "max_freq": 16}], "status": "Online", "description": "SatNOGS station_4"}',
    '{"station_name": "station_5", "station_id": "5", "antennas": [{"min_freq": 5, "max_freq": 16}, {"min_freq": 8, "max_freq": 16}], "status": "Online", "description": "SatNOGS station_5"}',
    '{"station_name": "station_6", "station_id": "6", "antennas": [{"min_freq": 6, "max_freq": 12}, {"min_freq": 9, "max_freq": 20}, {"min_freq": 8, "max_freq": 21}], "status": "Online", "description": "SatNOGS station_6"}',
    '{"station_name": "station_7", "station_id": "7", "antennas": [{"min_freq": 7, "max_freq": 14}], "status": "Online", "description": "SatNOGS station_7"}',
    '{"station_name": "station_8", "station_id": "8", "antennas": [{"min_freq": 8, "max_freq": 20}, {"min_freq": 9, "max_freq": 23}, {"min_freq": 12, "max_freq": 20}, {"min_freq": 14, "max_freq": 26}, {"min_freq": 12, "max_freq": 24}], "status": "Online", "description": "SatNOGS station_8"}',
    '{"station_name": "station_9", "station_id": "9", "antennas": [{"min_freq": 9, "max_freq": 23}], "status": "Online", "description": "SatNOGS station_9"}',
    '{"station_name": "station_10", "station_id": "10", "antennas": [{"min_freq": 10, "max_freq": 24}, {"min_freq": 10, "max_freq": 16}, {"min_freq": 14, "max_freq": 26}, {"min_freq": 10, "max_freq": 15}, {"min_freq": 18, "max_freq": 29}], "status": "Online", "description": "SatNOGS station_10"}',
    '{"station_name": "station_11", "station_id": "11", "antennas": [{"min_freq": 11, "max_freq": 21}, {"min_freq": 11, "max_freq": 24}, {"min_freq": 17, "max_freq": 30}], "status": "Online", "description": "SatNOGS station_11"}',
    '{"station_name": "station_12", "station_id": "12", "antennas": [{"min_freq": 12, "max_freq": 17}, {"min_freq": 12, "max_freq": 17}, {"min_freq": 14, "max_freq": 24}, {"min_freq": 12, "max_freq": 20}], "status": "Online", "description": "SatNOGS station_12"}',
    '{"station_name": "station_13", "station_id": "13", "antennas": [{"min_freq": 13, "max_freq": 20}, {"min_freq": 13, "max_freq": 27}, {"min_freq": 13, "max_freq": 26}, {"min_freq": 19, "max_freq": 30}], "status": "Online", "description": "SatNOGS station_13"}',
    '{"station_name": "station_14", "station_id": "14", "antennas": [{"min_freq": 14, "max_freq": 25}, {"min_freq": 17, "max_freq": 24}, {"min_freq": 14, "max_freq": 23}], "status": "Online", "description": "SatNOGS station_14"}',
    '{"station_name": "station_15", "station_id": "15", "antennas": [{"min_freq": 15, "max_freq": 27}, {"min_freq": 16, "max_freq": 28}, {"min_freq": 17, "max_freq": 26}, {"min_freq": 24, "max_freq": 35}], "status": "Online", "description": "SatNOGS station_15"}',
    '{"station_name": "station_16", "station_id": "16", "antennas": [{"min_freq": 16, "max_freq": 22}], "status": "Online", "description": "SatNOGS station_16"}',
    '{"station_name": "station_17", "station_id": "17", "antennas": [{"min_freq": 17, "max_freq": 22}, {"min_freq": 18, "max_freq": 23}, {"min_freq": 17, "max_freq": 32}, {"min_freq": 26, "max_freq": 39}], "status": "Online", "description": "SatNOGS station_17"}',
    '{"station_name": "station_18", "station_id": "18", "antennas": [{"min_freq": 18, "max_freq": 26}, {"min_freq": 21, "max_freq": 26}, {"min_freq": 24, "max_freq": 32}, {"min_freq": 21, "max_freq": 26}], "status": "Online", "description": "SatNOGS station_18"}',
    '{"station_name": "station_19", "station_id": "19", "antennas": [{"min_freq": 19, "max_freq": 28}, {"min_freq": 19, "max_freq": 34}, {"min_freq": 21, "max_freq": 34}], "status": "Online", "description": "SatNOGS station_19"}',
    '{"station_name": "station_20", "station_id": "20", "antennas": [{"min_freq": 20, "max_freq": 33}, {"min_freq": 23, "max_freq": 33}], "status": "Online", "description": "SatNOGS station_20"}'
]


app = FastAPI()

@app.get("/stations")
async def stations():
    stations = [json.loads(station.rstrip()) for station in stations_list]
    for _ in range(5):
        random_int = random.randint(0, len(stations) - 1)
        stations[random_int]["status"] = "Offline"
    return stations


@app.post("/send_message")
async def receive_message(msg: Message):
    return responses[random.randint(0, len(responses) - 1)]
