from django.test import TestCase
from links.models import Link

# Create your tests here.
class TestLink(TestCase):

    def setUp(self):
        Link.objects.create(name="test_link_test", frequency=5000)
    

    def test_link(self):
        link = Link.objects.get(name="test_link_test")

        self.assertEqual(link.frequency, 5000)
        self.assertEqual(str(link), "test_link_test")