from django.db import models

# Create your models here.

class Link(models.Model):
    name = models.CharField(max_length=100, blank=False)
    frequency = models.IntegerField(blank=False) # frequency in GHz

    def __str__(self):
        return self.name
