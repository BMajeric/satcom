from django.shortcuts import render, redirect, get_object_or_404
from satellites.models import Satellite
from .models import Communication
from django.contrib.auth.decorators import login_required
from users.models import CustomUser
from .forms import CommunicationForm
from links.models import Link
import random
import requests
import json
from datetime import datetime

MOCKUP_URL = "http://rugajuci-slaven.herokuapp.com/"
# Create your views here.
@login_required()
def sat_select_view(request):
  context = {
    'user': request.user,
    'satellites': Satellite.objects.all()
  }

  return render(request, 'satellites_select.html', context)

@login_required()
def stations_select_view(request, id):
  
  form = CommunicationForm(id, request.POST)
  if form.is_valid():
    sat_link = Link.objects.get(name=request.POST['link'])
    stations = json.loads(requests.get(MOCKUP_URL + "stations").text)
    
    matching_stations = []
    for station in stations:
      if station['status'] == 'Offline':
        continue
      for antenna in station["antennas"]:
        if sat_link.frequency in range(antenna["min_freq"], antenna["max_freq"], 1):
          matching_stations.append(station)
          break
    
    if not len(matching_stations) == 0:
      random_matching_station = random.choice(matching_stations)
      context = {
        'link':sat_link,
        'random_station':json.dumps(random_matching_station),
        'msg_text':request.POST['msg_text'],
        'sat_id': id,
        'stations': matching_stations,
        'user': request.user
      }
      return render(request,'stations_select.html', context)
      

    else:
      response_text = 'Unsuccessful communication'
      new_communication = Communication.objects.create(
        msg_text = request.POST['msg_text'],
        owner = CustomUser.objects.get(id=request.user.id),
        satellite = Satellite.objects.get(id=id),
        station = 'No matching station',
        link = sat_link.name,
        response = response_text,
        com_time = datetime.now()
      )
      return redirect('../../../')
  else:
    return redirect('/')


@login_required()
def communication_finish_view(request, id):
  if (request.POST):
    sat_link = Link.objects.get(name=request.POST['linkID'])
    matching_station = request.POST['station']
  
    if ("Random" in matching_station):
      station = json.loads(request.POST['randomStation'])
    else:

      l = matching_station.split(',')
      for i in range (len(l)):
        if ("station_name" in l[i]):
          temp = l[i].split(':')
          station_name = temp[1]
        if ("station_id" in l[i]):
          temp = l[i].split(':')
          station_id = temp[1]
      station_name = station_name.strip()
      station = {
        'station_name':station_name[1:len(station_name)-1],
        'station_id':station_id
      }
      
    msg = {
        "message": request.POST['msg'],
        "station_id": station["station_id"],
        "satellite_id": str(id)
      }
   
    response = requests.post(MOCKUP_URL + "send_message", json=msg)
    response_text = response.text
    response_code = response.status_code
    
    if (response_code != 200):
      response_text = 'Unsuccessful communication'
      new_communication = Communication.objects.create(
        msg_text = request.POST['msg'],
        owner = CustomUser.objects.get(id=request.user.id),
        satellite = Satellite.objects.get(id=id),
        station = 'No matching station',
        link = sat_link.name,
        response = response_text,
        com_time = datetime.now()
      )
      
    else:
      new_communication = Communication.objects.create(
        msg_text = request.POST['msg'],
        owner = CustomUser.objects.get(id=request.user.id),
        satellite = Satellite.objects.get(id=id),
        station = station['station_name'],
        link = sat_link.name,
        response = response_text,
        com_time = datetime.now()
      )

    return redirect('../../../')
  
@login_required()
def communication_create_view(request, id):
  form = CommunicationForm(id)
  context = {
        'form': form,
        'user': request.user,
        'satellite': Satellite.objects.get(id=id)
  }

  return render(request, "communication_create.html", context)


@login_required
def communication_details_view(request, id):
  
  obj = get_object_or_404(Communication, id=id)
  if (request.user == obj.owner or request.user.is_superuser):
    
    context = {
      'user': request.user,
      "object": obj
    }

    return render(request, 'communication_details_view.html', context)
  else:
    return redirect('/')
  
@login_required
def communication_delete(request, id):
  obj = get_object_or_404(Communication, id=id)
  if (request.user == obj.owner or request.user.is_superuser):
    if request.method == 'POST':
      obj.delete()
      return redirect('../../../')

    context = {
      'user': request.user,
      "object": obj
    }
    return render(request, 'communication_delete_view.html', context)
  else:
    return redirect('/')
