from django.db import models
from users.models import CustomUser
from satellites.models import Satellite


# Create your models here.
class Communication(models.Model):
    owner = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    msg_text = models.CharField(max_length=100, blank=False)
    satellite = models.ForeignKey(Satellite, on_delete=models.CASCADE)
    response = models.CharField(max_length=100, blank=True)
    station = models.CharField(max_length=100, blank=False)
    link = models.CharField(max_length=100, blank=False)
    com_time = models.DateTimeField(blank = True)

    def __str__(self):
        return "message: " + self.msg_text + ", " + "response: " + self.response
    
    
