from django import forms
from satellites.models import Satellite
from users.models import CustomUser
from links.models import Link

class CommunicationForm(forms.Form):
    def __init__(self, satellite_id, *args, **kwargs):
          # call standard __init__
          satellite = Satellite.objects.get(id=satellite_id)
          super().__init__(*args,**kwargs)
          #extend __init__
          self.fields['link'] = forms.ModelChoiceField(queryset=satellite.link,
                                                       required=True,
                                                       to_field_name='name',
                                                       label = "Link"
                                                      )
          
    msg_text = forms.CharField(max_length=100, required=True, label='Poruka')
    

    
    
