from django.test import TestCase
from communications.models import Communication
from users.models import CustomUser
from satellites.models import Satellite
from links.models import Link


# Create your tests here.
class TestCommunication(TestCase):

    def setUp(self):
        link = "link_1"
        user = CustomUser.objects.create(email="communication_test_user@gmail.com")
        satellite = Satellite.objects.create(owner=user, name="test_communication_melupo")

        Communication.objects.create(owner=user, msg_text="TEST", satellite=satellite, 
                                    response="melupo", station="test_station", link=link)
    

    def test_communication_params(self):
        communication = Communication.objects.get(msg_text="TEST")
        satellite = Satellite.objects.get(name="test_communication_melupo")
        link = "link_1"
        user = CustomUser.objects.get(email="communication_test_user@gmail.com")

        self.assertEqual(communication.owner, user)
        self.assertEqual(communication.satellite, satellite)
        self.assertEqual(communication.link, link)
        self.assertEqual(communication.station, "test_station")
        self.assertEqual(communication.response, "melupo")
        self.assertEqual(communication.msg_text, "TEST")
    

    def test_communication_response(self):
        communication = Communication.objects.get(msg_text="TEST")

        self.assertEqual(str(communication), "message: " + communication.msg_text + ", " 
                                            + "response: " + communication.response)
