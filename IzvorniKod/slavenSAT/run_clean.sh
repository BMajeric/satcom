#!/bin/bash

echo "open a new terminal and run: 'uvicorn mockup_server:app --reload' to run the mockup server"
bash delete_migrations_and_db.sh
bash setup_db.sh
python manage.py runserver 8080