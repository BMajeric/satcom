#!/bin/bash

python manage.py makemigrations
python manage.py migrate
python manage.py loaddata links.json
python manage.py createsuperuser
