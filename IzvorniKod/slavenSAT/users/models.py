from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from .managers import CustomUserManager
    

class CustomUser(AbstractBaseUser, PermissionsMixin):
# replace admin role by superuser
# drop enumeration for roles,
# just have boolean field for sat admin :)
    email = models.EmailField(_('email address'), unique=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False) # for admin dashboard
    is_sat_admin = models.BooleanField(default=False)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['is_sat_admin']

    objects = CustomUserManager()

    def __str__(self):
        return self.email
