from django.test import TestCase
from users.models import CustomUser

# Create your tests here.
class TestCustomUser(TestCase):
    
    def setUp(self):
        CustomUser.objects.create(email="regular@user.com")
        CustomUser.objects.create(email="sat@admin.com", is_staff=True, is_active=True)
        CustomUser.objects.create(email="inactive@user.com", is_active=False)
    
    def test_users(self):
        """Custom users are saved correctly"""
        regular_user = CustomUser.objects.get(email="regular@user.com")
        sat_admin = CustomUser.objects.get(email="sat@admin.com")
        inactive_user = CustomUser.objects.get(email="inactive@user.com")

        self.assertEqual(str(regular_user), "regular@user.com")
        self.assertEqual(regular_user.is_active, True)
        self.assertEqual(regular_user.is_staff, False)
        self.assertEqual(sat_admin.is_staff, True)
        self.assertEqual(inactive_user.is_active, False)
