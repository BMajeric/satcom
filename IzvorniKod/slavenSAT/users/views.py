from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout, update_session_auth_hash
from .forms import CustomUserCreationForm, MyPasswordChangeForm
from communications.models import Communication
from users.models import CustomUser
from django.views.generic import TemplateView
from django.core.mail import send_mail
# Create your views here.


@login_required()
def my_view(request):
  # After login redirect user to home page
  if (request.user.is_superuser):
    user_communications = list(Communication.objects.all())
  else:
    user_communications = list(Communication.objects.filter(owner=request.user.id))

  context = {
    'user': request.user,
    'user_communications': user_communications
  }
  return render(request, 'home.html', context)


@login_required()
def create_users_view(request):
  # After login if user superuser, render form for creating new user
  if not request.user.is_superuser:
    return redirect('../')
  else:
    form = CustomUserCreationForm(request.POST or None)
    if form.is_valid():
      form.save()
      return redirect('../')
    context = {
      'user': request.user,
      'form': form
    }
    return render(request, 'users_create.html', context)

@login_required()
def logout_view(request):
  logout(request)
  return redirect('/')

@login_required
def change_password(request):
  if (request.POST):
    form = MyPasswordChangeForm(request.user, request.POST)
  else:
    form = MyPasswordChangeForm(request.user)
  if form.is_valid():
    user = form.save()
    update_session_auth_hash(request, user)
    send_mail(
      subject="[SATCOM] Password change",
      message="Your password has been successfully changed!",
      from_email="slavensatcom@gmail.com",
      recipient_list=[request.user.email],
      fail_silently=False
    )
    return redirect('../')
  context = {
    'user': request.user,
    'form': form
  }
  return render(request, 'password.html', context)
