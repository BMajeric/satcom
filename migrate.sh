echo "Running migrations..."
cd IzvorniKod/slavenSAT
python manage.py makemigrations
python manage.py migrate
# python manage.py migrate --run-syncdb
python manage.py loaddata links.json
python manage.py collectstatic --no-input
python manage.py createsuperuser --is_sat_admin False --noinput
cd ../../